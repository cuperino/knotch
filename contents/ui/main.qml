/*
    SPDX-FileCopyrightText: 2020 Niccolò Venerandi <niccolo@venerandi.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

import QtQuick 2.4
import QtQuick.Layouts 1.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras

Rectangle {
    id: root
    
    property bool ntop: plasmoid.location === PlasmaCore.Types.TopEdge
    property bool nright: plasmoid.location === PlasmaCore.Types.RightEdge
    property bool nbottom: plasmoid.location === PlasmaCore.Types.BottomEdge
    property bool nleft: plasmoid.location === PlasmaCore.Types.LeftEdge
    
    property int notchWidth: plasmoid.configuration.notchWidth
    property int notchRadius: plasmoid.configuration.notchRadius < ((ntop || nbottom) ? root.height : root.width) / 2 ? plasmoid.configuration.notchRadius : ((ntop || nbottom) ? root.height : root.width) / 2
    property int reduceHeight: plasmoid.configuration.notchMargin

    Layout.minimumWidth:   notchWidth
    Layout.preferredWidth: Layout.minimumWidth
    Layout.maximumWidth:   Layout.minimumWidth

    Layout.minimumHeight: Layout.minimumWidth
    Layout.preferredHeight: Layout.minimumHeight
    Layout.maximumHeight: Layout.minimumHeight

    Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation
    Plasmoid.constraintHints: PlasmaCore.Types.CanFillArea
    color: "transparent"
    
    Rectangle {
    	anchors {
    		fill: parent
    		bottomMargin: ntop ? reduceHeight : 0
    		leftMargin: nright ? reduceHeight : 0
    		rightMargin: nleft ? reduceHeight : 0
    		topMargin: nbottom ? reduceHeight : 0
    	}
    	color: "black"
    	radius: notchRadius
    }
    Rectangle {
    	anchors {
		fill: parent
    		bottomMargin: ntop ? reduceHeight + notchRadius - 2 : 0
    		leftMargin: nright ? reduceHeight + notchRadius - 2 : 0
    		rightMargin: nleft ? reduceHeight + notchRadius - 2 : 0
    		topMargin: nbottom ? reduceHeight + notchRadius - 2 : 0
    	}
    	color: "black"
    }
} 
